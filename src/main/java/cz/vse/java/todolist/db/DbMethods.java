package cz.vse.java.todolist.db;

import cz.vse.java.todolist.logic.Tag;
import cz.vse.java.todolist.logic.Todo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DbMethods {
    /**
     * ****************INSERT METHODS*****************
     */
    //inserting data into db table task
    //we need to figure out how to set tag_id
    public static void insertTodo(String description, Integer tag_id, Integer priority) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "INSERT INTO task(description,tag_id, priority) VALUES (?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, description);
            ps.setInt(2, tag_id);
            ps.setInt(3, priority);
            ps.execute();
            System.out.println("Data has been inserted!");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    //inserting data into db table tag
    public static void insertTag(String tag_name) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "INSERT INTO tag(tag_name) VALUES (?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, tag_name);
            ps.execute();
            System.out.println("Data has been inserted!");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    /**
     * ****************SELECT METHODS*****************
     */
    //reading data from the db table task
    public static Map<Integer, Todo> fetchTodos() {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Todo todo;
        Map allTodos = new HashMap<>();

        System.out.println("Fetching todos..");
        try {
            String sql = "SELECT * FROM task";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                todo = new Todo(rs.getInt("task_id"), rs.getInt("tag_id"), rs.getString("description"), rs.getInt("resolved"), rs.getInt("priority"));
                allTodos.put(todo.getId(), todo);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return allTodos;
    }

    public static Map<Integer, Tag> fetchTags() {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Tag tag;
        Map allTags = new HashMap<>();

        System.out.println("Fetching tags");
        try {
            String sql = "SELECT * FROM tag";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                tag = new Tag(rs.getInt("tag_id"), rs.getString(("tag_name")));
                allTags.put(tag.getId(), tag);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return allTags;
    }

    /**
     * ****************UPDATE METHODS*****************
     */
    //updating attributes
    //we need to figure out how to set the priority number
    public static void updatePriority(Integer priority, Integer task_id) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE task set priority = ? WHERE task_id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, priority);
            ps.setInt(2, task_id);
            ps.execute();
            System.out.println("Data has been updated!");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    //updating resolve
    public static void updateResolved(Integer resolved, Integer task_id) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE task set resolved = ? WHERE task_id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, resolved);
            ps.setInt(2, task_id);
            ps.execute();
            System.out.println("Data has been updated!");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    //updating tag
    public static void updateTag(Integer tag_id, Integer task_id) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE task set tag_id = ? WHERE task_id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tag_id);
            ps.setInt(2, task_id);
            ps.execute();
            System.out.println("Data has been updated!");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    /**
     * ****************DELETE METHODS*****************
     */
    public static void deleteTodo(Integer task_id) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "DELETE from TASK where task_id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, task_id);
            ps.execute();
            System.out.println("Data has been deleted.");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteTag(Integer tag_id) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM tag WHERE tag_id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tag_id);
            ps.execute();
            System.out.println("Data has been deleted.");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * DB CLEAR METHODS
     */

    public static void deleteAllTags() {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM tag";
            ps = con.prepareStatement(sql);
            ps.execute();
            System.out.println("Data has been deleted.");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAllTodos() {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM task";
            ps = con.prepareStatement(sql);
            ps.execute();
            System.out.println("Data has been deleted.");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteIncrement() {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "DELETE FROM sqlite_sequence";
            ps = con.prepareStatement(sql);
            ps.execute();
            System.out.println("Data has been deleted.");
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        }
        try {
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
