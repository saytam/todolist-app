package cz.vse.java.todolist.main;

public interface ISubject {

    /***
     * Registers a new "follower"
     * @param observer - takes one observer as a parameter so it registers the right one
     */
    void register(IObserver observer);

    /***
     * Unregisters a follower
     * @param observer - takes one observer as a parameter so it unregisters the right one
     */
    void unregister(IObserver observer);

    /***
     * Lets followers know about a change, so they can update()
     */
    void notifyObservers();
}
