package cz.vse.java.todolist.main;

public interface IObserver {

    /***
     * When Subject notifies Observers about a change, they use this method to keep up to date
     */
    void update();
}
