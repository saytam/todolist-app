package cz.vse.java.todolist.constants;

public class PRIORITY {

    public static final int[] LEVELS = {1, 2, 3};

    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer THREE = 3;

}
