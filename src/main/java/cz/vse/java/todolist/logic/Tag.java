package cz.vse.java.todolist.logic;

public class Tag {

    public Tag(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    private Integer id;
    private String title;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
