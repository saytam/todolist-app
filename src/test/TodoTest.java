import cz.vse.java.todolist.logic.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TodoTest {
    private Todo todo;

    @BeforeEach
    public void setUp() {
        todo = new Todo(1, 1, "Not okay", 0, 1);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    void tagTests() {
        assertEquals(1, todo.getId());
        assertEquals("Not okay", todo.getDescription());
    }
}